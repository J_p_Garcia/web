<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Wok</title>
        <link rel="icon" href="imagenes/70795.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="wok.css">
        <link href="https://fonts.googleapis.com/css?family=Amaranth|Kaushan+Script|Kavoon" rel="stylesheet">
        
    </head>
    <body>
      <?php

@session_start();
if(isset($_POST['logout'])){
    unset($_SESSION["reg"]);
    
    setcookie('visita',0,time()+3600);
    session_destroy();
    header("Location: f_login.php");

}
?>
        <header>
            <section id="logo">
                <img src="imagenes/logo.png" alt="Logo Wok">
            </section>
            <section class="box effect1">
                <img src="<?php if(!empty($_SESSION['reg'])){echo "/Wok/imagenes/".$_SESSION["reg"][5];}else{echo "https://cops.usdoj.gov/html/dispatch/01-2013/images/no_ID.jpg"; }?>" width="50" height="50"  alt="">
                <p id="bienvenida"> <?php  if(empty($_SESSION["reg"])){echo"!Bienvenid@ Invitado!";}else{ echo "!Bienvenid@ ".$_SESSION["reg"][0]." (".$_SESSION["reg"][4].")!"; }   ?></p>

                <p id="fecha"><?php date_default_timezone_set('UTC'); echo date('l jS \of F Y h:i:s A');?></p>
                <?php echo isset($button)  ? $button  : null;?>
                <form action="" method="post"><?php  if(!empty($_SESSION["reg"])){echo"<input type='submit' value='Salir' name='logout' id='slir'>";}else{}?></form>


            </section>
        </header>
      <nav id="menu">
          <ul id="menu-closed">
              <li><a href="index.php">Home</a></li>
              <?php if(isset($_SESSION["reg"])){}else{echo" <li>"."<a href='f_login.php''>Login</a>"."</li>";} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='F_modificar.php''>Perfil</a>"."</li>";}}else{ } ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";}}else{ echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='mispedidos.php''>Mis Pedidos</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='#''>Gestion Usuario</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='wok.php''>wok</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='historiapedidos.php''>Historialpedido</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}}else{ echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}?>
              <li><a href="#menu-closed">&#215; Cerrar</a></li>
              <li><a href="#menu">&#9776; Menu</a></li>
          </ul>
      </nav>
        <section id="conte">
          
<form action="" method="post" id="form" enctype="multipart/form-data">
<table class="nueva">

    <h2>Editar Perfil</h2>
    <h4>Datos cuenta</h4>
    <tr>
        <th>Usuario:</th>
        <td><?php echo " <span id='siempre'>".$_SESSION['reg'][0]."</span>" ?></td>
        <td rowspan="5"  ><img src="<?php if(!empty($nombre_img)){echo "/web/img/".$nombre_img; }else{ echo "https://pbs.twimg.com/profile_images/1569965057/se_or_x.jpg";}?>" width="125" height="125" alt="" /></td>
    </tr>
    <tr>
        <th>Archivo</th>
        <td><input type="file" name="imagen" value="imagen" id="imagen" size="500"></td>
        <td><span class="error"><?php echo (isset($errorfile)) ? $errorfile : '';?><?php echo (isset($errorformat)) ? $errorformat : '';?><?php echo (isset($errortamaño)) ? $errortamaño : '';?></span></td>
    </tr>
    <tr>
        <th>nombre</th>
        <td><input type="text" value="<?php if(empty($_POST['nom'])){echo $_SESSION["reg"][2];}else{echo $_POST['nom'];}?> " name="nom"></td>
        <?php echo "<td><span class='error'>";echo(isset($nameerror)) ? $nameerror : ''; "</span></td>";?>
    </tr>
    <tr>
        <th>Email</th>
        <td><input type="text" value="<?php if(empty($_POST['email'])){echo $_SESSION["reg"][3];}else{echo $_POST['email'];}?>" name="email"></td>
        <?php echo "<td><span class='error'>";echo(isset($mailerror)) ? $mailerror : ''; "</span></td>";?>
    </tr>
    <tr>
        <th>Firma</th>
        <td><input type="text" value="<?php if(empty($_POST['usuario'])){echo $_SESSION["reg"][4];}else{echo $_POST['alias'];}?>" name="alias"></td>
        <?php echo "<td><span class='error'>";echo (isset($aliaserror)) ? $aliaserror : ''; "</span></td>";?>
    </tr>
    <tr>
        <th>Rango</th>
        <td><?php echo " <span id='siempre'>".$_SESSION['reg'][6]."</span>" ?></td>
    </tr>
</table>
<table>
    <h4>Cambio contraseña</h4>
    <tr>
        <th>Antigua contraseña:</th>
        <td><input type="password" name="oldpass"></td>
        <?php echo "<td><span class='error'>";echo(isset($passerr)) ? $passerr : ''; "</span></td>";?>
        <?php echo "<td><span class='error'>";echo(isset($errorbd)) ? $errorbd : ''; "</span></td>";?>

    </tr>
    <tr>
        <th>Nueva contraseña</th>
        <td><input type="password" name="newpass"><br></td>
        <?php echo "<td><span class='error'>";echo(isset($errorpass)) ? $errorpass : ''; "</span></td>";?>
    </tr>

    <tr>
        <th>Repita Contraseña</th>
        <td><input type="password" name="r_newpass"></td>
    </tr>
</table>

<input type="submit" value="AplicarCambios">
<input type="submit" value="Finalizar" name="finalizar">
</form>
        </section>
        <footer>
            <section id="txt">
                <p>Created by......Jp</p>
            </section>
            <section id="redes">
                <div id="twiter"><a href="#"><img src="imagenes/twitter.png" alt="Los Tejos" width="200" height="100" /></a></div>
                <div id="email"><a href="#"><img src="imagenes/google_plus.png" alt="Los Tejos" width="200" height="100" /></a></div>
                <div id="facebook"><a href="#"><img src="imagenes/facebook.png" alt="Los Tejos" width="200" height="100" /></a></div>
            </section>
        </footer>
        
    </body>
</html>


