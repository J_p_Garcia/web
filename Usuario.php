<?php

/**
 * Created by PhpStorm.
 * User: JuanPablo
 * Date: 01/02/2017
 * Time: 15:45
 */
class Usuario {
    private $nick;
    private $pass;
    private $nombre;
    private $email;
    private $firma;
    private $img;
    private $karma;


    function getnick() {
        return $this -> nick;

    }
     function getpass() {
        return $this -> pass;

    }
   
    function getnombre() {
        return $this -> nombre;

    }
    function getemail() {
        return $this -> email;

    }
    function getfirma() {
        return $this -> firma;

    }
    function getimg() {
        return $this -> img;

    }
    function getkarma() {
        return $this -> karma;

    }

    function setnick($nick) {
        $this -> nick = $nick;
    }
     function setpas($pass) {
        $this -> pass = $pass;
    }

    function setnombre($nombre) {
        $this -> nombre = $nombre;

    }
    function setemail($email) {
        $this -> email = $email;

    }
    function setfirma($firma) {
        $this -> firma = $firma;

    }
    function setimg($img) {
        $this -> img = $img;

    }
    function setkarma($karma) {
        $this -> karma = $karma;

    }

}
?>