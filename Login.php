<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Wok</title>
        <link rel="icon" href="imagenes/70795.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="wok.css">
        <link href="https://fonts.googleapis.com/css?family=Amaranth|Kaushan+Script|Kavoon" rel="stylesheet">
        
    </head>
    <body>
      <?php

@session_start();
if(isset($_POST['logout'])){
    unset($_SESSION["reg"]);
    
    setcookie('visita',0,time()+3600);
    session_destroy();
    header("Location: f_login.php");

}
?>
        <header>
            <section id="logo">
                <img src="imagenes/logo.png" alt="Logo Wok">
            </section>
            <section class="box effect1">
                <img src="<?php if(!empty($_SESSION['reg'])){echo "/Wok/imagenes/".$_SESSION["reg"][5];}else{echo "https://cops.usdoj.gov/html/dispatch/01-2013/images/no_ID.jpg"; }?>" width="50" height="50"  alt="">
                <p id="bienvenida"> <?php  if(empty($_SESSION["reg"])){echo"!Bienvenid@ Invitado!";}else{ echo "!Bienvenid@ ".$_SESSION["reg"][0]." (".$_SESSION["reg"][4].")!"; }   ?></p>

                <p id="fecha"><?php date_default_timezone_set('UTC'); echo date('l jS \of F Y h:i:s A');?></p>
                <?php echo isset($button)  ? $button  : null;?>
                <form action="" method="post"><?php  if(!empty($_SESSION["reg"])){echo"<input type='submit' value='Salir' name='logout' id='slir'>";}else{}?></form>


            </section>
        </header>
      <nav id="menu">
          <ul id="menu-closed">
              <li><a href="index.php">Home</a></li>
              <?php if(isset($_SESSION["reg"])){}else{echo" <li>"."<a href='f_login.php''>Login</a>"."</li>";} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='F_modificar.php''>Perfil</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";}}else{ echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='mispedidos.php''>Mis Pedidos</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='#''>Gestion Usuario</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='wok.php''>wok</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='historiapedidos.php''>Historialpedido</a>"."</li>";}}else{} ?>
              <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}}else{ echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}?>
              <li><a href="#menu-closed">&#215; Cerrar</a></li>
              <li><a href="#menu">&#9776; Menu</a></li>
          </ul>
      </nav>
        <section id="conte">
            <form class="registro" action="" method="post">
                <section id="head">
                    <h2>Registrarse</h2>
                    <p>Datos necesarios para llevar acabo el registro</p>
                </section>
                <section id="rest">
                <table>
                    <tr id="nick">
                        <th>Nickname <span class="obligado">*</span>: </th>
                        <td><input type="text" name="nick"></td>
                        <td><span class="error"><?php echo (isset($Usuerror)) ? $Usuerror : '';?><?php echo (isset($errorlogin)) ? $errorlogin : '';?></td>
                    </tr>
                    <tr id="pass">
                        <th>Contraseña <span class="obligado">*</span>: </th>
                        <td><input type="password" name="pass"></td>
                        <td><span class="error"> <?php echo (isset($passerr)) ? $passerr : '';?><?php echo (isset($usuarioerr)) ? $usuarioerr : '';?></td>
                    </tr>
                    
                    
                 
                    
                </table>
                <?php echo (isset($varr)) ? $varr : ''; ?>
                <span class="error" id="check"> </span>
                    <section id="boton">
                        <input type="submit" value="Login" name="Login" id="loging"<?php if(isset($var)!=true){$var="";}  if ($var >=4){ echo 'disabled="disabled"';} ?>>
                        <input type="submit" value="registrarse" name="f_registro">
                    </section>
                    </section>
                
            </form>
            
        </section>
        <footer>
            <section id="txt">
                <p>Created by......Jp</p>
            </section>
            <section id="redes">
                <div id="twiter"><a href="#"><img src="imagenes/twitter.png" alt="Los Tejos" width="200" height="100" /></a></div>
                <div id="email"><a href="#"><img src="imagenes/google_plus.png" alt="Los Tejos" width="200" height="100" /></a></div>
                <div id="facebook"><a href="#"><img src="imagenes/facebook.png" alt="Los Tejos" width="200" height="100" /></a></div>
            </section>
        </footer>
        
    </body>
</html>
