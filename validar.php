<?php

/**
 * Created by PhpStorm.
 * User: JuanPablo
 * Date: 24/01/2017
 * Time: 16:39
 */
class Validar
{
    public $error_clave;
    public $error_mail;
    public $error_alias;
    public $error_user;
    public $error_nom;
    public $error_terminos;

    public function validacion($user,$password,$confirm_pw,$email,$alias,$name) {
        $valid_usuario = $this->validar_campo_usuario($user);
        $valid_pass    = $this->validar_password($password,$confirm_pw);
        $valid_email   = $this->validar_email($email);
        $valid_alias   = $this->validar_campo_alias($alias);
        $valid_nombre  = $this->validar_campo_nombre($name);
        return $valid_usuario && $valid_pass && $valid_email && $valid_alias && $valid_nombre ?: false;
    }
    public function validarLogin($user,$password) {
        $valid_usuario    = $this->validar_campo_usuario   ($user);
        $valid_pass    = $this->validar_clave($password);

        return $valid_usuario && $valid_pass  ?true : false;
    }
     public function validarPerfil($email,$alias,$name) {
        $valid_email   = $this->validar_email($email);
        $valid_alias   = $this->validar_campo_alias($alias);
        $valid_nombre  = $this->validar_campo_nombre($name);
         return  $valid_email && $valid_alias && $valid_nombre ?: false;
     }
    public function validar_password($password,$confirm_pw) {
        return ($this->comparar_passwords($password,$confirm_pw) && $this->validar_clave($password)) ? true : false;
    }

    public function comparar_passwords($password,$confirm_pw) {
        if ($password === $confirm_pw) {
            return true;
        }
        $this->error_clave = 'los campos no son iguales ';
        return false;
    }

    public function validar_campo_usuario($user) {
        if(!empty($user))  {
            return true;
        }
        $this->error_user = "Campo vacio";
        return false;
    }
    public function validar_campo_alias($alias) {
        if(!empty($alias))  {
            return true;
        }
        $this->error_alias = "Campo vacio";
        return false;
    }
    public function validar_campo_nombre($nombre) {
        if(!empty($nombre))  {
            return true;
        }
        $this->error_nom = "Campo vacio";
        return false;
    }

    function validar_clave($clave){
        if(strlen($clave) < 4){
            $this->error_clave = "La clave debe tener al menos 4 caracteres";
            return false;
        }


        return true;
    }
    function validar_email($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){

            return true;
        }
        $this->error_mail = "Campo Vacio o formato invalido";
        return false;
    }
    function sacarPrecio($id){
        if($id==1){
            $precio=7.50;
            return $precio;
        }
         if($id==2){
            $precio=6.95;
            return $precio;
        }
        if($id==3){
            $precio=7.95;
            return $precio;
        }

        return false;
    }

}
