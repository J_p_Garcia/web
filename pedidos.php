<?php

/**
 * Created by PhpStorm.
 * User: JuanPablo
 * Date: 01/02/2017
 * Time: 15:45
 */
class Pedidos {
    private $idpedido;
    private $login;
    private $idbase;
    private $numingr;
    private $ingr;
    private $data;
    private $servido;


    function getidpedido() {
        return $this -> idpedido;

    }
     function getlogin() {
        return $this -> login;

    }
   
    function getidbase() {
        return $this -> idbase;

    }
    function getnumingr() {
        return $this -> numingr;

    }
    function getingr() {
        return $this -> ingr;

    }
    function getdata() {
        return $this -> data;

    }
    function getservido() {
        return $this -> servido;

    }

    function setidpedido($idpedido) {
        $this -> idpedido = $idpedido;
    }
     function setlogin($login) {
        $this -> login = $login;
    }

    function setidbase($idbase) {
        $this -> idbase = $idbase;

    }
    function setnumingr($numingr) {
        $this -> numingr = $numingr;

    }
    function setingr($ingr) {
        $this -> ingr = $ingr;

    }
    function setdata($data) {
        $this -> data = $data;

    }
    function setservido($servido) {
        $this -> servido = $servido;

    }

}
?>