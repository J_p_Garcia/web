<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Wok</title>
        <link rel="icon" href="imagenes/70795.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="wok.css">
        <link href="https://fonts.googleapis.com/css?family=Amaranth|Kaushan+Script|Kavoon" rel="stylesheet">
    </head>
    <body>
    <?php
    header('Content-Type: text/html; charset=UTF-8');

@session_start();
echo '<pre>' . var_export($_SESSION["reg"], true) . '</pre>';
if(isset($_POST['logout'])){
    unset($_SESSION["reg"]);
    
    setcookie('visita',0,time()+3600);
    session_destroy();
    header("Location: f_login.php");

}

?>
       <header>
            <section id="logo">
                <img src="imagenes/logo.png" alt="Logo Wok">
            </section>
            <section class="box effect1">
                <img src="<?php if(!empty($_SESSION['reg'])){echo "/Wok/imagenes/".$_SESSION["reg"][5];}else{echo "https://cops.usdoj.gov/html/dispatch/01-2013/images/no_ID.jpg"; }?>" width="50" height="50"  alt="">
                <p id="bienvenida"> <?php  if(empty($_SESSION["reg"])){echo"!Bienvenid@ Invitado!";}else{ echo "!Bienvenid@ ".$_SESSION["reg"][0]." (".$_SESSION["reg"][4].")!"; }   ?></p>

                <p id="fecha"><?php date_default_timezone_set('UTC'); echo date('l jS \of F Y h:i:s A');?></p>
                <?php echo isset($button)  ? $button  : null;?>
                <form action="" method="post"><?php  if(!empty($_SESSION["reg"])){echo"<input type='submit' value='Salir' name='logout' id='slir'>";}else{}?></form>


            </section>
        </header>

    <nav id="menu">
        <ul id="menu-closed">
            <li><a href="index.php">Home</a></li>
            <?php if(isset($_SESSION["reg"])){}else{echo" <li>"."<a href='f_login.php''>Login</a>"."</li>";} ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='F_modificar.php''>Perfil</a>"."</li>";}}else{ } ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";}}else{ echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";} ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='mispedidos.php''>Mis Pedidos</a>"."</li>";}}else{} ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='#''>Gestion Usuario</a>"."</li>";}}else{} ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='wok.php''>wok</a>"."</li>";}}else{} ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='historiapedidos.php''>Historialpedido</a>"."</li>";}}else{} ?>
            <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}}else{ echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}?>
            <li><a href="#menu-closed">&#215; Cerrar</a></li>
            <li><a href="#menu">&#9776; Menu</a></li>
        </ul>
    </nav>
        <section id="conte">

            <article id="bases">
                <h2>Bases para WOK</h2>
                <section class="arroz">
                <h4>Arroz</h4>
                      <img src="imagenes/base%20arroz.jpg" alt="Smiley face" height="128" width="128">
                <p>Tal vez este tipo de plato hecho en el wok sea uno de los más populares y tradicionales. El wok de arroz está hecho principalmente a base de arroz, que se acompaña con unas verduras para hacerlo más rico y completo. Aquí tienes la receta detallada paso a paso. 
</p>
                     <p class="precio">Precio.........7.95?</p>
                  
                </section>
                <section class="verduras">
                <h4>Verduras</h4>
                     <img src="imagenes/WokDeVerdurasYPollo.jpg" alt="Smiley face" imagenes/WokDeVerdurasYPollo.jpgheight="128" width="128">
                <p>Este wok de verduras es uno de los platos con más éxito de los últimos tiempos, dónde se ha puesto de moda cocinar con el wok, un utensilio muy tradicional de la gastronomía oriental que nos permite cocinar de forma saludable. 
                </p><br>
                <p class="precio">Precio.........7.95?</p>
                   
                </section>
                <section class="fideos">
                <h4>noodles</h4>
                    <img src="imagenes/WokDeFideos.jpg" alt="Smiley face" height="128" width="128">
                <p>Esta es una receta para preparar un delicioso wok de fideos. Para hacerla vamos a necesitar fideos de arroz, un alimento muy rico y saludable de gran tradición en la cocina china y oriental en general 
                </p><br>
                <p class="precio">Precio.........7.95?</p>
                </section>
            </article>
            <article id="Ingredientes">
              <ul class="food"> <h3>Ingredientess</h3>
                  <li >Brotes de soja</li>
                  <li>Champiñones</li>
                  <li>Pollo</li>
                  <li>Gambas</li>
                  <li>Ternera</li>
                  <li>Guisantes</li>
                  <li>Espárragos</li>
                  <li>Pimiento</li>
                  <li>Cebolla</li>
                  <li>Brócoli</li>
                  <li>Brotes de soja</li>
              </ul>
                <ul class="salsas"><h3>Salsas</h3>
                    <li>Salsa teriyaki</li>
                    <li>Salsa de soja</li>
                    <li>Salsa de curry</li>
                </ul>
            </article>
        </section>
        <footer>
            <section id="txt">
                <p>Created by......Jp</p>
            </section>
            <section id="redes">
                <div id="twiter"><a href="#"><img src="imagenes/twitter.png" alt="Los Tejos" width="200" height="100" /></a></div>
                <div id="email"><a href="#"><img src="imagenes/google_plus.png" alt="Los Tejos" width="200" height="100" /></a></div>
                <div id="facebook"><a href="#"><img src="imagenes/facebook.png" alt="Los Tejos" width="200" height="100" /></a></div>
            </section>
        </footer>
        
    </body>
</html>
