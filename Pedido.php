<?php
include'Base.php';
include'Ingredientes.php';
header('Content-Type: text/html; charset=ISO-8859-1');
/**


 * Created by PhpStorm.
 * User: JuanPablo
 * Date: 10/02/2017
 * Time: 17:25
 */
session_start();

if(empty($_SESSION['reg'])){
    echo '<script language="javascript">alert("Solo los usuarios registrados pueden realizar pedidos");';
    echo "window.location.href='index.php';";
    echo "</script>";

}
?>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wok</title>
    <link rel="icon" href="imagenes/70795.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="wok.css">
    <link href="https://fonts.googleapis.com/css?family=Amaranth|Kaushan+Script|Kavoon" rel="stylesheet">

</head>
<body>
<?php

@session_start();
if(isset($_POST['logout'])){
    unset($_SESSION["reg"]);

    setcookie('visita',0,time()+3600);
    session_destroy();
    header("Location: f_login.php");

}

?>
<header>
            <section id="logo">
                <img src="imagenes/logo.png" alt="Logo Wok">
            </section>
            <section class="box effect1">
                <img src="<?php if(!empty($_SESSION['reg'])){echo "/Wok/imagenes/".$_SESSION["reg"][5];}else{echo "https://cops.usdoj.gov/html/dispatch/01-2013/images/no_ID.jpg"; }?>" width="50" height="50"  alt="">
                <p id="bienvenida"> <?php  if(empty($_SESSION["reg"])){echo"!Bienvenid@ Invitado!";}else{ echo "!Bienvenid@ ".$_SESSION["reg"][0]." (".$_SESSION["reg"][4].")!"; }   ?></p>

                <p id="fecha"><?php date_default_timezone_set('UTC'); echo date('l jS \of F Y h:i:s A');?></p>
                <?php echo isset($button)  ? $button  : null;?>
                <form action="" method="post"><?php  if(!empty($_SESSION["reg"])){echo"<input type='submit' value='Salir' name='logout' id='slir'>";}else{}?></form>


            </section>
        </header>
<nav id="menu">
    <ul id="menu-closed">
        <li><a href="index.php">Home</a></li>
        <?php if(isset($_SESSION["reg"])){}else{echo" <li>"."<a href='f_login.php''>Login</a>"."</li>";} ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='F_modificar.php''>Perfil</a>"."</li>";}}else{ } ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";}}else{ echo" <li>"."<a href='Pedido.php''>Mis Pedidos</a>"."</li>";} ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='mispedidos.php''>Mis Pedidos</a>"."</li>";}}else{} ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='#''>Gestion Usuario</a>"."</li>";}}else{} ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='wok.php''>wok</a>"."</li>";}}else{} ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==2)){echo" <li>"."<a href='historiapedidos.php''>Historialpedido</a>"."</li>";}}else{} ?>
        <?php if(isset($_SESSION["reg"][6])){if(!empty($_SESSION["reg"][6]==1)){echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}}else{ echo" <li>"."<a href='Contacto.php''>Contacto</a>"."</li>";}?>
        <li><a href="#menu-closed">&#215; Cerrar</a></li>
        <li><a href="#menu">&#9776; Menu</a></li>
    </ul>
</nav>

<section id="conte">
    <form action="" method="post">
    <section class="bases">
    <h2>Elige la base</h2>

        <?php

        $conn = new mysqli('localhost', 'root', '','wok');
        $sql = "SELECT * FROM `bases`";
        $resultado=$conn->query($sql);
        $nfilas = $resultado->num_rows;
        if ($resultado){


            if ($nfilas > 0){
                for ($i=0; $i<$nfilas; $i++){
                    $fila=$resultado->fetch_array();

                    $base =new Base();

                    $base->setidbase($fila[0]);
                    $base->setdrescripcion($fila[1]);
                    $base->setprecio($fila[2]);
                    $dolar=$base->getprecio();
                    $id=$base->getidbase();
                    $txt=$base->getdrescripcion();

                    echo " <input type='radio' name='base' value='$id'><label>$txt.....$dolar</label>"."<br>";

                }

            }
            $conn->close();
        }

        ?>
    </section>
    <section class="ingredietes">
    <h2>Ingredientes Extras</h2>
        <?php
        $conn = new mysqli('localhost', 'root', '','wok');
        $sql = "SELECT * FROM `ingredientes`";
        $resultado=$conn->query($sql);
        $nfilas = $resultado->num_rows;
        if ($resultado){


            if ($nfilas > 0){
                for ($i=0; $i<$nfilas; $i++){
                    $fila=$resultado->fetch_array();

                    $ing =new Ingredientes();

                    $ing->setnombre($fila[0]);
                    $ing->setdrescripcion($fila[1]);

                    $nom=$ing->getnombre();
                    $txt=$ing->getdrescripcion();

                    echo"<input type='checkbox' name='Ingredientes[]' value='$nom'><label>$nom</label>"."<br>";

                }

            }
            $conn->close();
        }?>

   </section>
    <input type="submit" name="pedido"  id="sendpedido" >
        <?php

        if(isset($_POST['pedido'])){
            if(isset($_POST['base'])){
                //print ($sexo);
                $id=$_POST ['base'];
                $selected='';
                echo $id;
                $current = 0;
                $conn = new mysqli('localhost', 'root', '','wok');
                $fecha=strftime( "%d/%m/%Y a las %H:%M:%S", time() );
                $asd=$_SESSION["reg"][0];

                $conn->query($sql);
                if (is_array($_POST['Ingredientes'])) {
                    $selected = '';
                    $num_countries = count($_POST['Ingredientes']);
                    $current = 0;
                    foreach ($_POST['Ingredientes'] as $key => $value) {
                        if ($current != $num_countries-1)
                            $selected .= $value.', ';
                        else
                            $selected .= $value.'.';
                        $current++;
                    }

                }
                $sql = "INSERT INTO `pedidos` (login, idBase,numIng,ingredientes, fechayhora,servido)
                     VALUES ('$asd', '$id','$current','$selected','$fecha' ,'0')";
                $conn->query($sql);
            }else{
              $radio= 'Debes seleccionar Una base';
                echo $radio;
            }


            echo '<div>Has seleccionado: '.$selected.'</div>';
        }

        ?>
    </form>
</section>
<footer>
   <section id="txt">
       <p>Created by......Jp</p>
   </section>
   <section id="redes">
       <div id="twiter"><a href="#"><img src="imagenes/twitter.png" alt="Los Tejos" width="200" height="100" /></a></div>
       <div id="email"><a href="#"><img src="imagenes/google_plus.png" alt="Los Tejos" width="200" height="100" /></a></div>
       <div id="facebook"><a href="#"><img src="imagenes/facebook.png" alt="Los Tejos" width="200" height="100" /></a></div>
   </section>
</footer>

</body>
</html>
